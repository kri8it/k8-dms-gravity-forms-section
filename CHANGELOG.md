## CHANGELOG

= 1.0 =
* Initial release 

= 1.0.1 =
* K8 Updater compatibility

= 1.0.2 =
* Updated default submit button class to btn-link-color