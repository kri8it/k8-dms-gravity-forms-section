<?php
/*
	Plugin Name: K8 Gravity Forms
	Plugin URI: http://www.kri8it.com
	Description: Add a Gravity Form anywhere
	Author: Charl Pretorius
	PageLines: true
	Version: 1.0.2
	Section: true
	Class Name: K8GForm
	Filter: component, forms
	Loading: active

	K8 Bitbucket Plugin URI: https://bitbucket.org/kri8it/k8-dms-gravity-forms-section
*/

/**
 * IMPORTANT
 * This tells wordpress to not load the class as DMS will do it later when the main sections API is available.
 * If you want to include PHP earlier like a normal plugin just add it above here.
 */

if( ! class_exists( 'PageLinesSectionFactory' ) )
	return;

if( ! class_exists( 'GFForms' ) )
	return;
	
class K8GForm extends PageLinesSection {
	
	function section_persistent(){
        
		// filter the Gravity Forms button type
		add_filter( 'gform_submit_button', array( $this, 'form_submit_button' ), 10, 2 );
		
    }
	
	function form_submit_button($button, $form){
		
		$theme = $this->opt( 'k8_gform_submit_button', array( 'default' => 'btn-link-color' ) );
		
		return "<button class='btn {$theme} btn-large' id='gform_submit_button_{$form["id"]}'><span>{$form['button']['text']}</span></button>";
		
	}
	
	function section_template() {
			
		$title = $this->opt( 'k8_gform_title', array( 'default' => 'Contact Us' ) );
		$title_wrap = $this->opt( 'k8_gform_title_wrap', array( 'default' => 'h2' )  );
		$show_description = $this->opt( 'k8_gform_show_description' );
		$use_ajax = $this->opt( 'k8_gform_ajax' );
		$form = $this->opt( 'k8_gform_form' );
		
		if( $form != '' ):
			
			if( $title != '' ):
				
				$form_title = sprintf( '<%s class="form-head" data-sync="k8_gform_title">%s</%s>', $title_wrap, $title, $title_wrap );
				echo $form_title;
				
			endif;
			
			gravity_form( $form, false, $show_description, false, null, $use_ajax );
			
		else:
			
			echo '<p>Please select a form...</p>';
				
		endif;
		
	}

	function section_opts(){
		
		// Get all Gravity Forms
		$forms = RGFormsModel::get_forms( null, 'title' );		
		
		foreach( $forms as $form ):
			
			$forms_array[$form->id] = array( 'name' => $form->title );         
			
		endforeach;
		
		$opts = array(
			array(
				'type'		=> 'multi',
				'key'		=> 'k8_gform_settings',
				'col'		=> 1,
				'opts'		=> array(
					array(
						'key'			=> 'k8_gform_title',
						'type' 			=> 'text',
						'label' 		=> __( 'Form Title', 'pagelines' ),
					),
					array(
						'type' 			=> 'select',
						'key'			=> 'k8_gform_title_wrap',
						'label' 		=> __( 'Form Title Text Wrapper', 'pagelines' ),
						'default'		=> 'h2',
						'opts'			=> array(
							'h1'			=> array('name' => '&lt;h1&gt;'),
							'h2'			=> array('name' => '&lt;h2&gt;  (default)'),
							'h3'			=> array('name' => '&lt;h3&gt;'),
							'h4'			=> array('name' => '&lt;h4&gt;'),
							'h5'			=> array('name' => '&lt;h5&gt;'),
						)
					),
					array(
						'key'			=> 'k8_gform_show_description',						
						'label'			=> __( 'Show Description', 'pagelines' ),
						'type'			=> 'check',
						'default'		=> false
					),
					array(
						'key'			=> 'k8_gform_ajax',						
						'label'			=> __( 'Use Ajax', 'pagelines' ),
						'type'			=> 'check',
						'default'		=> false
					),
					array(
						'type'          => 'select',
		                'title'         => 'Select Form',
		                'key'           => 'k8_gform_form',
		                'label'         => 'Gravity Form',
		                'opts'			=> $forms_array
					),
					array(
						'key'			=> 'k8_gform_submit_button',
						'type' 			=> 'select_button',
						'label'			=> __( 'Button Color', 'pagelines' ),
					),								
				)
			)
		);
		return $opts;		
	}
}